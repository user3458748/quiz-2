<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('car/create', 'CarController@create')->name('car.create');
Route::post('car/handle', 'CarController@handle')->name('car.handle');
Route::get('car/show', 'CarController@show')->name('car.show');

Route::get('animal/create', 'AnimalController@create')->name('animal.create');
Route::get('animal/handle', 'AnimalController@handle')->name('animal.handle');



Route::get('pod', function () {
    return view('styles.pod');
});

Route::get('example', function () {
    return view('examples.create');
});

Route::get('grid', function () {
    return view('styles.grid');
});






