@extends('layouts.master')

@section('content')

    <div align="center">
        <h1>Animal Form</h1>
        <form action="{{ route('car.handle') }}" method="POST">
            {{csrf_field()}}
            <label>
                Name:
            </label>
            <input type="text" name="name">
            <br>
            <label>
                Age:
            </label>
            <input type="number" name="age">
            <br>
            <input type="submit">
        </form>
@endsection