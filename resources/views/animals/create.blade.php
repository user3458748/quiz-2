@extends('layouts.master')

@section('content')

    <div align="center">
        <h1>Animal Form</h1>
        <form action="{{ route('animal.handle') }}" method="GET">
            {{csrf_field()}}
            <label>
                Type:</label>
            <input type="text" name="type">
            <br>
            <label>
                Price:
            </label>
            <input type="number" name="price"><br>
            <input type="submit">
        </form>
    </div>
    
@endsection