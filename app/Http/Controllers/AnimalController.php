<?php

namespace App\Http\Controllers;

/**
 * Class AnimalController
 * @package App\Http\Controllers
 */
class AnimalController extends Controller
{

    /**
     * Open form for animal
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('animals.create');
    }

    /**
     * Handle and show form results
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function handle()
    {
        $type = $_GET['type'];
        $price = $_GET['price'];

        return view('animals.show', [$type, $price]);
    }

}









