<?php

namespace App\Http\Controllers;

/**
 * Class CarController
 * @package App\Http\Controllers
 */
class CarController
{
    /**
     * Handle and show form results
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function handle()
    {
        $name = $_POST['name'];
        $age = $_POST['age'];

        return view('cars.show', [$name, $age]);

    }

    /**
     * Open form for car
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('cars.create');
    }

}